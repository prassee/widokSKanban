package skanban

import org.widok._
import org.widok.html._
import org.widok.bindings.Bootstrap._
import java.net.URLConnection
import java.net.URL
import scala.scalajs.js.annotation.JSExport
import org.scalajs.dom.raw.XMLHttpRequest
import org.scalajs.dom.raw.Event

object Main extends PageApplication {

  case class Card(name: String)

  type card = Var[String]
  val prodName = "SKanban"
  val cards = Buffer[Ref[Card]]()
  val jsonResult = Var("")
  val hasCards = cards.nonEmpty
  val name = Var("")
  val hasName = name.map(_.nonEmpty)

  val weather = Var("")
  val hasWeather = weather.nonEmpty

  val navbar = NavigationBar(Container(
    NavigationBar.Header(NavigationBar.Toggle(), NavigationBar.Brand(prodName)), NavigationBar.Collapse()))

  val todoPanel = div(Panel(Panel.Heading(Panel.Title3("Todo")),
    Panel.Body(ul(cards.map { card => li(card.get.name) }))).style(Style.Info).show(hasCards)).css("col-md-4")

  val doingPanel = div(Panel(Panel.Heading(Panel.Title3("Doing")),
    Panel.Body(ul(cards.map { card => li(card.get.name) }))).style(Style.Primary).show(hasCards)).css("col-md-4")

  val donePanel = div(Panel(Panel.Heading(Panel.Title3("Done")),
    Panel.Body(ul(cards.map { card => li(card.get.name) }))).style(Style.Warning).show(hasCards)).css("col-md-4")

  val leftPanel = div(label("Enter URL").forId("card"),
    Input.Text().placeholder("CardName").id("card").bind(name),
    br(),
    Button("Create").css("btn-primary").onClick(x => {
      cards.+=(Ref(Card(name.get)))
      name := ""
      cards.foreach { x => log(x.get.name) }
      log("no of cards " + cards.size + "cards -> " + cards)
    }),
    span(" "),
    Button("Clear").onClick(x => {
      log("no of cards " + cards.size)
      cards.clear()
    })).css("col-md-3")

  val rightPanel = div(todoPanel, doingPanel, donePanel).css("col-md-9")

  def view() = {
    Inline(navbar, Container(
      leftPanel,
      rightPanel  ))
  }
  def ready() {}
}

